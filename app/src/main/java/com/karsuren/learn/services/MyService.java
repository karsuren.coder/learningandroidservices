package com.karsuren.learn.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class MyService extends Service {
    int mVar;

    public MyService() {
        Log.d("surenkar-Myservice", "in constructor");
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d("surenkar-Myservice", "in onBind()");
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("surenkar-Myservice", "in onCreate()");
        mVar = 0;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("surenkar-Myservice", "in onStartCommand()");
        mVar += 1;
        Log.d("surenkar-Myservice", "mVar = "+mVar);
        if (mVar == 10) {
            stopSelf();
            Log.d("surenkar-Myservice", "executing rest of the onStartCommand() method");
        }
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("surenkar-Myservice", "in onDestroy()");
    }
}